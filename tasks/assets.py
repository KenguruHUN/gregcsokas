# -*- coding: utf-8 -*-
from invoke import task


@task
def push_to_container(ctx, volume="gregcsokas-nginx"):
    """
    Push static files to the named volume.

    :param ctx: contextualized=true boilerplate of Invoke

    :param volume: named volume
    :type volume: string

    :return: None
    """

    ctx.run('docker cp assets/* {volume}:/var/www/html/gregcsokas'.format(volume=volume))
