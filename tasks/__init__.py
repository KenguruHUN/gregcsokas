# -*- coding: utf-8 -*-
from invoke import Collection

from . import assets
from . import build
from . import docker
from . import dev

ns = Collection()

ns.add_collection(Collection.from_module(assets))
ns.add_collection(Collection.from_module(build))
ns.add_collection(Collection.from_module(dev))
ns.add_collection(Collection.from_module(docker))

