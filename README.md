Gregcsokas
============

Blog and portfolio engine
-------------------------

This an engine of my blog and portfolio site, I'm restart this project about 4 or more times ... 
hopeless, but I hope I will do it once :D

#### Versioning ####

major.minor.maitenance

(number).(yy.mmdd).(commitnum)

##### examples #####

* ###### 0.17.1019.0 ######
  This is a zeroth major version of the package released in 2017-10-19,
  commited changes are 0.
  
* ###### 0.17.1019.33 ######
  This is again a zeroth major version of the package released the same day as before
  but we have 33 committed changes.
  
##### Installation #####



Schematic diagram:
------------------
![](docs/source/_static/img/schema.svg "Schematic")