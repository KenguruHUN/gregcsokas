# -*- coding: utf-8 -*-
import logging
from .base import *

logger = logging.getLogger(__name__)

DEBUG = env('DEBUG')

SESSION_EXPIRE_AT_BROWSER_CLOSE = False

INTERNAL_IPS = ['127.0.0.1', '192.168.1.118', '172.17.0.1', '172.20.0.1']

INSTALLED_APPS += [
    'debug_toolbar',
    'template_profiler_panel',
    'template_timings_panel',
    'requests_toolbar',
]

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static_root'),
    os.path.join(BASE_DIR, 'assets')
]


DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'requests_toolbar.panels.RequestsDebugPanel',
    'template_profiler_panel.panels.template.TemplateProfilerPanel',
    'template_timings_panel.panels.TemplateTimings.TemplateTimings',
    'debug_toolbar.panels.profiling.ProfilingPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]

DEBUG_TOOLBAR_CONFIG = {
    'RENDER_PANELS': True,
    'SHOW_TOOLBAR_CALLBACK': lambda request: True,
    'INTERCEPT_REDIRECTS': False,
    'SQL_WARNING_THRESHOLD': 100,  # milliseconds
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    },

    "session": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://0.0.0.0:6379/2",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient"
        },
    }
}
