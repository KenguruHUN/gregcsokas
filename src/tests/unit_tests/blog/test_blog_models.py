from datetime import datetime

import pytest
from pytz import UTC

from blog.models import Category, Tag, Post

pytestmark = pytest.mark.django_db


class TestCreateCategories:

    def test_simple_category(self, category_1):
        category = Category.objects.first()

        assert category.name == "Test Category 1"
        assert category.slug == "test-category-1"

    def test_simple_category_with_accents(self, category_2_with_accents):
        category = Category.objects.first()

        assert category.name == "Teszt Kategória 2"
        assert category.slug == "teszt-kategoria-2"


class TestCreateTag:

    def test_simple_tag(self, tag_1):
        tag = Tag.objects.first()

        assert tag.name == 'test'

    def test_tag_with_accents(self, tag_2):
        tag = Tag.objects.first()

        assert tag.name == 'ékezetes'

    def test_multiple_tag(self, tag_1, tag_2, tag_3):
        tag = Tag.objects.all()

        assert tag[0].name == 'ékezetes'
        assert tag[1].name == 'test2'
        assert tag[2].name == 'test'


class TestCreatePost:

    def test_post_1_published(self, post_1_published, category_1):
        post = Post.objects.first()

        assert post.title == 'Test Post 1'
        assert post.category == category_1
        assert post.status == Post.STATUSES.published
        assert post.pub_date == datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)

    def test_post_2_draft(self, post_2_draft, category_1):
        post = Post.objects.first()

        assert post.title == 'Test Post 2'
        assert post.category == category_1
        assert post.status == Post.STATUSES.draft
        assert post.pub_date == datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)

    def test_post_3_private(self, post_3_private, category_1):
        post = Post.objects.first()

        assert post.title == 'Test Post 3'
        assert post.category == category_1
        assert post.status == Post.STATUSES.private
        assert post.pub_date == datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)

    def test_post_4_published(self, post_4_published_pub_date_tomorrow, category_1):
        post = Post.objects.first()

        assert post.title == 'Test Post 4'
        assert post.category == category_1
        assert post.status == Post.STATUSES.published
        assert post.pub_date == datetime(2007, 12, 6, 12, 00, 00, tzinfo=UTC)

    def test_post_5_draft(self, post_5_draft_pub_date_tomorrow, category_1):
        post = Post.objects.first()

        assert post.title == 'Test Post 5'
        assert post.category == category_1
        assert post.status == Post.STATUSES.draft
        assert post.pub_date == datetime(2007, 12, 6, 12, 00, 00, tzinfo=UTC)

    def test_post_6_private(self, post_6_private_pub_date_tomorrow, category_1):
        post = Post.objects.first()

        assert post.title == 'Test Post 6'
        assert post.category == category_1
        assert post.status == Post.STATUSES.private
        assert post.pub_date == datetime(2007, 12, 6, 12, 00, 00, tzinfo=UTC)

