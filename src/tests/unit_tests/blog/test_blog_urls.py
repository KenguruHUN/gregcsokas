import pytest
from django.urls import reverse, resolve

pytestmark = pytest.mark.django_db


class TestUrls:

    def test_blog_index_url(self):
        path = reverse('blog:index')
        assert resolve(path).view_name == 'blog:index'

    def test_blog_index_url_proper_function_call(self):
        path = reverse('blog:index')
        assert resolve(path).func.__name__ == 'BlogIndexView'

    def test_blog_category_url(self, category_1):
        path = reverse('blog:category', kwargs={'category': category_1.slug})
        assert resolve(path).view_name == 'blog:category'

    def test_blog_category_url_proper_function_call(self, category_1):
        path = reverse('blog:category', kwargs={'category': category_1.slug})
        assert resolve(path).func.__name__ == 'PostByCategoryListView'
