# -*- coding: utf-8 -*-
import pytest
from datetime import datetime, timezone
from datetime import timedelta
from django.utils.timezone import now
from pytz import UTC

from blog.models import Category, Tag, Post


@pytest.fixture
def category_1():
    return Category.objects.create(name='Test Category 1')


@pytest.fixture
def category_1_with_accents():
    return Category.objects.create(name='Teszt Kategória 1')


@pytest.fixture
def category_2():
    return Category.objects.create(name='Test Category 2')


@pytest.fixture
def category_2_with_accents():
    return Category.objects.create(name='Teszt Kategória 2')


@pytest.fixture
def category_3():
    return Category.objects.create(name='Test Category 3')


@pytest.fixture
def tag_1():
    return Tag.objects.create(name='test')


@pytest.fixture
def tag_2():
    return Tag.objects.create(name='ékezetes')


@pytest.fixture
def tag_3():
    return Tag.objects.create(name='test2'),


@pytest.fixture
def post_1_published(user_2_admin, category_1):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)
    return Post.objects.create(author=user_2_admin, category=category_1, content="this is the post content",
                               title="Test Post 1", pub_date=pub_date, status=Post.STATUSES.published)


@pytest.fixture
def post_2_draft(user_2_admin, category_1):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)
    return Post.objects.create(author=user_2_admin, category=category_1, content="this is the post content",
                               title="Test Post 2", pub_date=pub_date, status=Post.STATUSES.draft)


@pytest.fixture
def post_3_private(user_2_admin, category_1):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)
    return Post.objects.create(author=user_2_admin, category=category_1, content="this is the post content",
                               title="Test Post 3", pub_date=pub_date, status=Post.STATUSES.private)


@pytest.fixture
def post_1A_published(user_2_admin, category_2):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)
    return Post.objects.create(author=user_2_admin, category=category_2, content="this is the post content",
                               title="Test Post 1", pub_date=pub_date, status=Post.STATUSES.published)


@pytest.fixture
def post_2A_draft(user_2_admin, category_2):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)
    return Post.objects.create(author=user_2_admin, category=category_2, content="this is the post content",
                               title="Test Post 2", pub_date=pub_date, status=Post.STATUSES.draft)


@pytest.fixture
def post_3A_private(user_2_admin, category_2):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)
    return Post.objects.create(author=user_2_admin, category=category_2, content="this is the post content",
                               title="Test Post 3", pub_date=pub_date, status=Post.STATUSES.private)

@pytest.fixture
def post_1B_published(user_2_admin, category_3):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)
    return Post.objects.create(author=user_2_admin, category=category_3, content="this is the post content",
                               title="Test Post 1", pub_date=pub_date, status=Post.STATUSES.published)


@pytest.fixture
def post_2B_draft(user_2_admin, category_3):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)
    return Post.objects.create(author=user_2_admin, category=category_3, content="this is the post content",
                               title="Test Post 2", pub_date=pub_date, status=Post.STATUSES.draft)


@pytest.fixture
def post_3B_private(user_2_admin, category_3):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC)
    return Post.objects.create(author=user_2_admin, category=category_3, content="this is the post content",
                               title="Test Post 3", pub_date=pub_date, status=Post.STATUSES.private)


@pytest.fixture
def post_4_published_pub_date_tomorrow(user_2_admin, category_1):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC) + timedelta(days=1)
    return Post.objects.create(author=user_2_admin, category=category_1, content="this is the post content",
                               title="Test Post 4", pub_date=pub_date, status=Post.STATUSES.published)


@pytest.fixture
def post_5_draft_pub_date_tomorrow(user_2_admin, category_1):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC) + timedelta(days=1)
    return Post.objects.create(author=user_2_admin, category=category_1, content="this is the post content",
                               title="Test Post 5", pub_date=pub_date, status=Post.STATUSES.draft)


@pytest.fixture
def post_6_private_pub_date_tomorrow(user_2_admin, category_1):
    pub_date = datetime(2007, 12, 5, 12, 00, 00, tzinfo=UTC) + timedelta(days=1)
    return Post.objects.create(author=user_2_admin, category=category_1, content="this is the post content",
                               title="Test Post 6", pub_date=pub_date, status=Post.STATUSES.private)
