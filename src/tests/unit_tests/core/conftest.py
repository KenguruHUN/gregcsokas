# -*- coding: utf-8 -*-
import pytest

from core.models import SinglePage, MetaData


@pytest.fixture
def default_index_page():
    return SinglePage.objects.create(filter_url="/",
                                     title="Default index page",
                                     content="# Title \n Test content")


@pytest.fixture
def default_about_page():
    return SinglePage.objects.create(filter_url="/about/",
                                     title="Default about page",
                                     content="# AboutTest \n About content")


@pytest.fixture
def default_meta_without_filter_url():
    return MetaData.objects.create(filter_url=None,
                                   title="Test title",
                                   description="Test description",
                                   keywords=['test', 'keywords', 'pytest'])


@pytest.fixture
def default_meta():
    return MetaData.objects.create(filter_url="",
                                   title="Test title",
                                   description="Test description",
                                   keywords=['test', 'keywords', 'pytest'])


@pytest.fixture
def index_meta():
    return MetaData.objects.create(filter_url="/",
                                   title="Index title",
                                   description="Index description",
                                   keywords=['index', 'test', 'keywords', 'pytest'])


@pytest.fixture
def about_meta():
    return MetaData.objects.create(filter_url="/about/",
                                   title="About title",
                                   description="About description",
                                   keywords=['about', 'test', 'keywords', 'pytest'])
