
from django.urls import reverse, resolve


class TestUrls:

    def test_index_url(self):
        path = reverse('index')
        assert resolve(path).view_name == 'index'

    def test_index_url_function(self):
        path = reverse('index')
        assert resolve(path).func.__name__ == 'SinglePageView'

    def test_about_url(self):
        path = reverse('about')
        assert resolve(path).view_name == 'about'

    def test_about_url_function(self):
        path = reverse('about')
        assert resolve(path).func.__name__ == 'SinglePageView'
