# -*- coding: utf-8 -*-
import pytest
from django.urls import reverse
from core.models import MetaData

pytestmark = pytest.mark.django_db


def test_metadata_exist(default_meta):
    metadata = MetaData.objects.first()

    assert metadata.filter_url == ""
    assert metadata.title == "Test title"
    assert metadata.description == "Test description"
    assert metadata.keywords == ['test', 'keywords', 'pytest']


class TestSimplePageView:

    def test__index__page_with__default_meta__(self, user_1_client, default_meta, default_index_page):
        request = user_1_client.get(reverse('index'))

        assert request.content.decode("utf-8").__contains__('<title>Test title</title>')
        assert request.content.decode("utf-8").__contains__('<meta name="description" content="Test description" />')  # noqa
        assert request.content.decode("utf-8").__contains__('<meta name="keywords" content="test, keywords, pytest" />')  # noqa
        assert request.content.decode("utf-8").__contains__("<h1>Title</h1>\n<p>Test content</p>")

    def test__index__page_with__default_meta__filter_url__none(self, user_1_client, default_meta_without_filter_url, default_index_page):
        request = user_1_client.get(reverse('index'))

        assert request.content.decode("utf-8").__contains__('<title>Test title</title>')
        assert request.content.decode("utf-8").__contains__('<meta name="description" content="Test description" />')  # noqa
        assert request.content.decode("utf-8").__contains__('<meta name="keywords" content="test, keywords, pytest" />')  # noqa
        assert request.content.decode("utf-8").__contains__("<h1>Title</h1>\n<p>Test content</p>")

    def test__index__page_with__page_specific_meta__(self, user_1_client, index_meta, default_index_page):
        request = user_1_client.get(reverse('index'))

        assert request.content.decode("utf-8").__contains__('<title>Index title</title>')
        assert request.content.decode("utf-8").__contains__('<meta name="description" content="Index description" />')  # noqa
        assert request.content.decode("utf-8").__contains__('<meta name="keywords" content="index, test, keywords, pytest" />')  # noqa
        assert request.content.decode("utf-8").__contains__("<h1>Title</h1>\n<p>Test content</p>")

    def test__about__page_with__default_meta__(self, user_1_client, default_meta, default_about_page):
        request = user_1_client.get(reverse('about'))

        assert request.content.decode("utf-8").__contains__('<title>Test title</title>')
        assert request.content.decode("utf-8").__contains__('<meta name="description" content="Test description" />')  # noqa
        assert request.content.decode("utf-8").__contains__('<meta name="keywords" content="test, keywords, pytest" />')  # noqa
        assert request.content.decode("utf-8").__contains__("<h1>AboutTest</h1>\n<p>About content</p>")

    def test__about__page_with__default_meta__filter_url__none(self, user_1_client, default_meta_without_filter_url, default_about_page):
        request = user_1_client.get(reverse('about'))

        assert request.content.decode("utf-8").__contains__('<title>Test title</title>')
        assert request.content.decode("utf-8").__contains__('<meta name="description" content="Test description" />')  # noqa
        assert request.content.decode("utf-8").__contains__('<meta name="keywords" content="test, keywords, pytest" />')  # noqa
        assert request.content.decode("utf-8").__contains__("<h1>AboutTest</h1>\n<p>About content</p>")

    def test__about__page_with__page_specific_meta__(self, user_1_client, about_meta, default_about_page):
        request = user_1_client.get(reverse('about'))

        assert request.content.decode("utf-8").__contains__('<title>About title</title>')
        assert request.content.decode("utf-8").__contains__('<meta name="description" content="About description" />')  # noqa
        assert request.content.decode("utf-8").__contains__('<meta name="keywords" content="about, test, keywords, pytest" />')  # noqa
        assert request.content.decode("utf-8").__contains__("<h1>AboutTest</h1>\n<p>About content</p>")

