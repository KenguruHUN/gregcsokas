# -*- coding: utf-8 -*-
import pytest
from django.contrib.auth.models import User
from django.test import RequestFactory, Client


@pytest.fixture
def rf():
    return RequestFactory()


@pytest.fixture
def cl():
    return Client()


@pytest.fixture
def user_1():
    return User.objects.create_user(username='userin_bolt',
                                    email='userin_bolt@mailprovider.org.hu',
                                    password='abcd1234')


@pytest.fixture
def user_2_admin():
    return User.objects.create_superuser(username='mary_poppins',
                                         email='mary@poppins-mail.org.hu',
                                         password='12345abcde')


@pytest.fixture
def user_1_client(cl, user_1):
    cl.login(user=user_1)
    return cl


@pytest.fixture
def user_2_admin_client(cl, user_2_admin):
    cl.login(user=user_2_admin)
    return cl
