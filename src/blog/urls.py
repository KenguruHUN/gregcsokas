# -*- coding: utf-8 -*-
from django.urls import path

from blog import views

from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

app_name = 'blog'

urlpatterns = [
    path('', cache_page(CACHE_TTL)(views.BlogIndexView.as_view()), name='index'),
    path('<category>/', cache_page(CACHE_TTL)(views.PostByCategoryListView.as_view()), name='category'),
    path('<category>/<slug:slug>', cache_page(CACHE_TTL)(views.PostDetailView.as_view()), name='detail'),
]