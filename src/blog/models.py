from django.db import models
from model_utils import Choices
from django.utils import timezone
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from martor.models import MartorField


class Category(models.Model):
    """ Category model."""

    name = models.CharField(_('category name'), max_length=50)
    slug = models.SlugField(_('category slug'), max_length=50, blank=True)

    class Meta:
        verbose_name_plural = 'Categories'
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super(Category, self).save(*args, **kwargs)


class Tag(models.Model):
    """Tag model."""

    name = models.CharField(_('tag'), max_length=25, blank=True)

    class Meta:
        ordering = ['-name']

    def __str__(self):
        return self.name


class TimeStampModel(models.Model):
    """ Time stamp abstract model."""

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class ContentModel(TimeStampModel):
    """ Content abstract model."""

    content = MartorField(_('content'))

    class Meta:
        abstract = True


class Post(ContentModel):
    """Post model."""

    STATUSES = Choices(
        (0, 'draft', _('draft')),
        (1, 'published', _('published')),
        (2, 'private', _('private')),
    )

    author = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to={'is_staff': True},
                               null=True, related_name='blog_posts')
    category = models.ForeignKey('Category', on_delete=models.CASCADE)
    tags = models.ManyToManyField('Tag')
    title = models.CharField(_('post title'), max_length=100)
    slug = models.SlugField(_('post slug'), max_length=105, blank=True, unique=True)
    head_image = models.ImageField(upload_to='uploads/', default='no-image')
    status = models.IntegerField(choices=STATUSES, default=STATUSES.draft, db_index=True)
    pub_date = models.DateTimeField(_('publication date'), default=timezone.now)
    comments = models.BooleanField(_('allow comments'), default=True)
    view_count = models.IntegerField(_('sum of views'), default=0)
    favs = models.IntegerField(_('sum of favourites'), default=0)

    class Meta:
        unique_together = ('slug', 'created')
        ordering = ['-pub_date']

    objects = models.Manager()

    @property
    def is_public(self):
        if self.status is Post.STATUSES.published and not self.pub_date > timezone.now():
            return True
        else:
            return False

    def get_absolute_url(self):
        return "{}/{}".format(self.category.slug, self.slug)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        return super(Post, self).save(*args, **kwargs)

