# -*- coding: utf-8 -*-
from .models import Post


def blog_category_links(request):
    """
    Query posts, if user is staff get all of the posts,
    if user is not staff get all public posts.
    """

    request_user = request.user.is_staff

    if request.path is "/lab/":
        pass
    else:
        memo = {}
        queryset = Post.objects.select_related('category').distinct()

        def get_categories():
            if not memo.get('categories'):
                memo['categories'] = set([post.category for post in queryset.iterator() if post.is_public or request_user])
            return memo['categories']

        return {'categories': get_categories}
