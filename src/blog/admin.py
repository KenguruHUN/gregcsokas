from django.contrib import admin

from blog.models import Category, Tag, Post


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'pub_date', 'status')


admin.site.register(Tag)
admin.site.register(Category)
admin.site.register(Post, PostAdmin)
