# -*- coding: utf-8 -*-
import logging
from django.views import generic

from blog.models import Post


logger = logging.getLogger('gregcsokas' + __name__)


class BlogIndexView(generic.ListView):
    """
    Blog index view which is return with all blog posts.
    Except draft or private posts if the user is not in staff group
    """

    template_name = 'post_list.html'
    model = Post
    context_object_name = 'posts'
    queryset = Post.objects.select_related('category').select_related('author').prefetch_related('tags').distinct()

    def get_queryset(self):

        posts = [post for post in self.queryset.iterator() if post.is_public or self.request.user.is_staff]

        return posts

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        context['categories'] = set(post.category for post in self.get_queryset())

        return context


class PostByCategoryListView(generic.ListView):
    """
    Post list view which is return with category specific post.
    Except draft or private posts if the user is not in staff group
    """

    template_name = 'post_list.html'
    model = Post
    context_object_name = 'posts'
    queryset = Post.objects.select_related('category').select_related('author').prefetch_related('tags').distinct()

    def get_queryset(self, **kwargs):
        queryset = self.queryset.filter(category__slug=self.kwargs['category'])

        posts = [post for post in queryset.iterator() if post.is_public or self.request.user.is_staff]

        return posts

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        context['categories'] = set(post.category for post in BlogIndexView.get_queryset(self))

        return context


class PostDetailView(generic.DetailView):
    """ """

    template_name = 'post_detail.html'
    model = Post
    queryset = Post.objects.all()

    def get_object(self):
        obj = super().get_object()

        return obj
