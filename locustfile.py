# -*- coding: utf-8 -*-
import random

from locust import HttpLocust, TaskSet, task


def login(l):
    l.client.post("/login", {"username": "kenguru", "password": "education"})


class UserBehavior(TaskSet):

    def on_start(self):
        login(self)

    @task
    def visit_index(self):
        response = self.client.get("/")
        print("Response status code:", response.status_code)
        print("Response content:", response.text)

    @task(2)
    def visit_blog(self):
        response = self.client.get("/lab")
        print("Response status code:", response.status_code)
        print("Response content:", response.text)

    @task(1)
    def visit_category_1(self):
        self.client.get("/lab/html/")

    @task(1)
    def visit_category_2(self):
        self.client.get("/lab/angular/")

    @task(1)
    def visit_category_3(self):
        self.client.get("/lab/beagle-bone/")


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 1000
    max_wait = 9950
    wait_function = lambda self: random.expovariate(1) * 1000
